package semestre.octavo.aplicaciones_moviles_prueba

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import semestre.octavo.aplicaciones_moviles_prueba.R

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}
